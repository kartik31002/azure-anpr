resource "azurerm_resource_group" "example" {
  name     = "${var.name}-rg"
  location = var.location
}

resource "azurerm_virtual_network" "main" {
  name                = "${var.name}-vnet"
  address_space       = var.vnet_address_space
  location            = azurerm_resource_group.example.location
  resource_group_name = azurerm_resource_group.example.name
}

resource "azurerm_subnet" "internal" {
  name                 = var.subnet_name
  resource_group_name  = azurerm_resource_group.example.name
  virtual_network_name = azurerm_virtual_network.main.name
  address_prefixes     = var.subnet_address_prefix
}

resource "azurerm_public_ip" "public_ip" {
  name                = "${var.name}-public_ip"
  resource_group_name = azurerm_resource_group.example.name
  location            = azurerm_resource_group.example.location
  allocation_method   = "Static"
}

# data "azurerm_public_ip" "public_ip"{
#   name = azurerm_public_ip.public_ip.name
#   resource_group_name = azurerm_public_ip.public_ip.resource_group_name
# }

resource "azurerm_network_interface" "example" {
  name                = "${var.name}-nic"
  location            = azurerm_resource_group.example.location
  resource_group_name = azurerm_resource_group.example.name

  ip_configuration {
    name                          = var.nic_ipconfig_name
    subnet_id                     = azurerm_subnet.internal.id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id          = azurerm_public_ip.public_ip.id
  }
}

resource "azurerm_network_security_group" "nsg" {
  name                = var.security_group_name
  location            = azurerm_resource_group.example.location
  resource_group_name = azurerm_resource_group.example.name

  security_rule {
    name                       = "allow_ssh_sg"
    priority                   = 100
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "22"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }
}

resource "azurerm_network_interface_security_group_association" "association" {
  network_interface_id      = azurerm_network_interface.example.id
  network_security_group_id = azurerm_network_security_group.nsg.id
}

resource "azurerm_linux_virtual_machine" "main" {
  name                = "${var.name}-vm"
  location            = azurerm_resource_group.example.location
  resource_group_name = azurerm_resource_group.example.name
  size                = var.vm_size
  admin_username      = var.vm_admin_username
  custom_data         = base64encode(file("./iot_edge_runtime.sh"))
  depends_on          = [azurerm_iothub.example]
  network_interface_ids = [
    azurerm_network_interface.example.id,
  ]
  disable_password_authentication = false

  admin_password = var.vm_admin_password

  os_disk {
    caching              = var.os_disk_caching
    storage_account_type = var.os_disk_sa_type
  }

  source_image_reference {
    publisher = var.sir_publisher
    offer     = var.sir_offer
    sku       = var.sir_sku
    version   = var.sir_version
  }

  tags = var.tags

  provisioner "remote-exec" {
    inline = ["sleep 2m", "sudo iotedge config mp --connection-string ${file("output.txt")}", "sudo iotedge config apply", ]

    connection {
      type     = "ssh"
      user     = self.admin_username
      password = self.admin_password
      host     = self.public_ip_address
    }
  }

}

#data "template_file" "linux-vm-cloud-init" {
#  template = file("iot_edge_runtime.sh")
#}

resource "azurerm_iothub" "example" {
  name                = "${var.name}-IoTHub"
  resource_group_name = azurerm_resource_group.example.name
  location            = azurerm_resource_group.example.location
  sku {
    name     = var.iothub_sku_name
    capacity = var.iothub_sku_capacity
  }

  provisioner "local-exec" {
    command = "az login --service-principal -u $ARM_CLIENT_ID -p $ARM_CLIENT_SECRET --tenant $ARM_TENANT_ID"
  }

  provisioner "local-exec" {
    command = "az iot hub device-identity create --device-id ${var.iot_edgedevice} --edge-enabled --hub-name ${self.name}"
  }
  provisioner "local-exec" {
    command = "echo -n \"'\" >> output.txt"
  }
  
  provisioner "local-exec" {
    command = "echo -n $(az iot hub device-identity connection-string show --device-id ${var.iot_edgedevice} --hub-name ${self.name} -o tsv) >> output.txt"
  }
  provisioner "local-exec" {
    command = "echo -n \"'\" >> output.txt"
  }  
  provisioner "local-exec" {
    when = destroy
    command = "truncate -s 0 output.txt"
  }    
}

