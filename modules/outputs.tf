output "vm_id" {
  description = "ID of the VM created"
  value       = azurerm_linux_virtual_machine.main.id
}

output "public_ip" {
  description = "IP address of the VM created"
  value       = azurerm_linux_virtual_machine.main.public_ip_address
}

output "iothub" {
  description = "Name of the IOT Hub created"
  value       = azurerm_iothub.example.name
}

output "edgedevice" {
  description = "Name of the iot edge device registered"
  value = var.iot_edgedevice
}