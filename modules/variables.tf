variable "name" {
  description = "Name to be used on all resources as prefix"
  type     = string
}

variable "location" {
  description = "Location of all resources"
  type = string
}

variable "vnet_address_space" {
  description   = "Virtual Network address space"
  type = list(string)
}

variable "subnet_name" {
  description   = "Subnet Name"
  type = string
}

variable "subnet_address_prefix" {
  description   = "Subnet address prefix"
  type = list(string)
}

variable "security_group_name" {
  description   = "Security group Name"
  type = string
}

variable "nic_ipconfig_name" {
  description   = "NIC ip configuration Name"
  type = string
}

variable "vm_size" {
  description   = "VM Size"
  type = string
}

variable "vm_admin_username" {
  description   = "Username to login into the vm"
  type = string
}

variable "vm_admin_password" {
  description   = "Password to login into the vm"
  type = string
}

variable "os_disk_caching" {
  description   = "The caching configuration of the os disk"
  type = string
}

variable "os_disk_sa_type" {
  description   = "Storage account type of the os disk"
  type = string
}

variable "sir_publisher" {
  description   = "Source Image Reference publisher"
  type = string
}

variable "sir_offer" {
  description   = "Source Image Reference offer"
  type = string
}

variable "sir_sku" {
  description   = "Source Image Reference sku"
  type = string
}

variable "sir_version" {
  description   = "Source Image Reference version"
  type = string
}

variable "tags" {
  description   = "Tags to assign to the resource"
  type = map(string)
}

variable "iothub_sku_name" {
  description   = "IOT hub sku name"
  type = string
}

variable "iothub_sku_capacity" {
  description   = "IOT hub sku capacity"
  type = string
}

variable "iot_edgedevice" {
  description   = "Name of the IOT edge device to be registered on the IOT hub"
  type = string
}