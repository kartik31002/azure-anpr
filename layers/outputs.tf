output "vm_id" {
  description = "ID of the VM created"
  value = module.azurerm.vm_id
}

output "public_ip" {
  description = "IP address of the VM created"
  value = module.azurerm.public_ip
}

output "iothub" {
  description = "Name of the IOT Hub created"
  value = module.azurerm.iothub
}

output "edgedevice" {
  description = "Name of the iot edge device registered"
  value = var.iot_edgedevice
}

