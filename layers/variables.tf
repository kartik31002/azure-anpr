variable "name" {
  description = "Name to be used on all resources as prefix"
  #default     = "ANPR"
}

variable "location" {
  description = "Location of all resources"
  #default = "Central India"
}

variable "vnet_address_space" {
  description   = "Virtual Network address space"
  default       = ["10.0.0.0/16"]
}

variable "subnet_name" {
  description   = "Subnet Name"
  #default       = "Internal"
}

variable "subnet_address_prefix" {
  description   = "Subnet address prefix"
  default       = ["10.0.2.0/24"]
}

variable "security_group_name" {
  description   = "Security group Name"
  #default       = "ssh_nsg"
}

variable "nic_ipconfig_name" {
  description   = "NIC ip configuration Name"
  #default       = "ipconfig"
}

variable "vm_size" {
  description   = "VM Size"
  #default       = "Standard_F2"
}

variable "vm_admin_username" {
  description   = "Username to login into the vm"
  #default       = "adminuser"
}

variable "vm_admin_password" {
  description   = "Password to login into the vm"
 # default = "edgeRocks@007"
}

variable "os_disk_caching" {
  description   = "The caching configuration of the os disk"
  default       = "ReadWrite"
}

variable "os_disk_sa_type" {
  description   = "Storage account type of the os disk"
  default       = "Standard_LRS"
}

variable "sir_publisher" {
  description   = "Source Image Reference publisher"
  default       = "Canonical"
}

variable "sir_offer" {
  description   = "Source Image Reference offer"
  default       = "0001-com-ubuntu-server-focal"
}

variable "sir_sku" {
  description   = "Source Image Reference sku"
  default       = "20_04-lts"
}

variable "sir_version" {
  description   = "Source Image Reference version"
  default       = "latest"
}

variable "tags" {
  description   = "Tags to assign to the resource"
  default       = {
    environment = "test"
  }
}

variable "iothub_sku_name" {
  description   = "IOT hub sku name"
  default       = "S1"
}

variable "iothub_sku_capacity" {
  description   = "IOT hub sku capacity"
  default       = "1"
}

variable "iot_edgedevice" {
  description   = "Name of the IOT edge device to be registered on the IOT hub"
  #default = "demoedgedevice"
}