module "azurerm" {
  source = "../modules"

  name                  = var.name
  location              = var.location
  vnet_address_space    = var.vnet_address_space
  subnet_name           = var.subnet_name
  subnet_address_prefix = var.subnet_address_prefix
  security_group_name   = var.security_group_name
  nic_ipconfig_name     = var.nic_ipconfig_name
  vm_size               = var.vm_size
  vm_admin_username     = var.vm_admin_username
  vm_admin_password     = var.vm_admin_password
  os_disk_caching       = var.os_disk_caching
  os_disk_sa_type       = var.os_disk_sa_type
  sir_publisher         = var.sir_publisher
  sir_offer             = var.sir_offer
  sir_sku               = var.sir_sku
  sir_version           = var.sir_version
  tags                  = var.tags
  iothub_sku_name       = var.iothub_sku_name
  iothub_sku_capacity   = var.iothub_sku_capacity
  iot_edgedevice        = var.iot_edgedevice
}