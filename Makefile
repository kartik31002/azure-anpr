################################################
#makefile

LOCATION               = layers
STATEBUCKET            = new-terraform-bucket
STATEREGION            = us-east-1
STATEKEY               = terraform/azure.tfstate

ifndef LOCATION
$(error LOCATION is not set)
endif

.PHONY: plan

first-run:
	@echo "initialize terraform statefile"
	cd $(LOCATION) && \
	rm -r .terraform/ || true && \
	export ARM_SUBSCRIPTION_ID=$(ARM_SUBSCRIPTION_ID) && \
    export ARM_TENANT_ID=$(ARM_TENANT_ID) && \
    export ARM_CLIENT_ID=$(ARM_CLIENT_ID) && \
    export ARM_CLIENT_SECRET=$(ARM_CLIENT_SECRET) && \
	export AWS_ACCESS_KEY_ID=$(AWS_ACCESS_KEY_ID) && \
    export AWS_SECRET_ACCESS_KEY=$(AWS_SECRET_ACCESS_KEY) && \
    export TF_VAR_name=$(name) TF_VAR_location=$(location) TF_VAR_subnet_name=$(subnet_name) TF_VAR_security_group_name=$(security_group_name) TF_VAR_nic_ipconfig_name=$(nic_ipconfig_name) TF_VAR_vm_size=$(vm_size) TF_VAR_vm_admin_username=$(vm_admin_username) TF_VAR_vm_admin_password=$(vm_admin_password) TF_VAR_iot_edgedevice=$(iot_edgedevice)

init:
	@echo "initialize terraform statefile and workspace"
	cd $(LOCATION) && \
    export ARM_SUBSCRIPTION_ID=$(ARM_SUBSCRIPTION_ID) && \
    export ARM_TENANT_ID=$(ARM_TENANT_ID) && \
    export ARM_CLIENT_ID=$(ARM_CLIENT_ID) && \
    export ARM_CLIENT_SECRET=$(ARM_CLIENT_SECRET) && \
	export AWS_ACCESS_KEY_ID=$(AWS_ACCESS_KEY_ID) && \
    export AWS_SECRET_ACCESS_KEY=$(AWS_SECRET_ACCESS_KEY) && \
    export TF_VAR_name=$(name) TF_VAR_location=$(location) TF_VAR_subnet_name=$(subnet_name) TF_VAR_security_group_name=$(security_group_name) TF_VAR_nic_ipconfig_name=$(nic_ipconfig_name) TF_VAR_vm_size=$(vm_size) TF_VAR_vm_admin_username=$(vm_admin_username) TF_VAR_vm_admin_password=$(vm_admin_password) TF_VAR_iot_edgedevice=$(iot_edgedevice) && \
    terraform init -upgrade -reconfigure -backend-config="bucket=$(STATEBUCKET)" -backend-config="key=$(STATEKEY)" -backend-config="region=$(STATEREGION)"

validate: init
	@echo "running terraform validate"
	cd $(LOCATION) && \
    terraform validate -no-color

plan: validate
	@echo "running terraform plan"
	cd $(LOCATION) && \
	export ARM_SUBSCRIPTION_ID=$(ARM_SUBSCRIPTION_ID) && \
    export ARM_TENANT_ID=$(ARM_TENANT_ID) && \
    export ARM_CLIENT_ID=$(ARM_CLIENT_ID) && \
    export ARM_CLIENT_SECRET=$(ARM_CLIENT_SECRET) && \
    export TF_VAR_name=$(name) TF_VAR_location=$(location) TF_VAR_subnet_name=$(subnet_name) TF_VAR_security_group_name=$(security_group_name) TF_VAR_nic_ipconfig_name=$(nic_ipconfig_name) TF_VAR_vm_size=$(vm_size) TF_VAR_vm_admin_username=$(vm_admin_username) TF_VAR_vm_admin_password=$(vm_admin_password) TF_VAR_iot_edgedevice=$(iot_edgedevice) && \
    terraform plan -no-color
	@echo "plan completed"

apply: plan
	@echo "running terraform apply"
	cd $(LOCATION) && \
    export ARM_SUBSCRIPTION_ID=$(ARM_SUBSCRIPTION_ID) && \
    export ARM_TENANT_ID=$(ARM_TENANT_ID) && \
    export ARM_CLIENT_ID=$(ARM_CLIENT_ID) && \
    export ARM_CLIENT_SECRET=$(ARM_CLIENT_SECRET) && \
    export TF_VAR_name=$(name) TF_VAR_location=$(location) TF_VAR_subnet_name=$(subnet_name) TF_VAR_security_group_name=$(security_group_name) TF_VAR_nic_ipconfig_name=$(nic_ipconfig_name) TF_VAR_vm_size=$(vm_size) TF_VAR_vm_admin_username=$(vm_admin_username) TF_VAR_vm_admin_password=$(vm_admin_password) TF_VAR_iot_edgedevice=$(iot_edgedevice) && \
    terraform apply  -auto-approve -no-color

plan-destroy: validate
	@echo "running terraform plan-destroy"
	cd $(LOCATION) && \
    export ARM_SUBSCRIPTION_ID=$(ARM_SUBSCRIPTION_ID) && \
    export ARM_TENANT_ID=$(ARM_TENANT_ID) && \
    export ARM_CLIENT_ID=$(ARM_CLIENT_ID) && \
	export ARM_CLIENT_SECRET=$(ARM_CLIENT_SECRET) && \
	export TF_VAR_name=$(name) TF_VAR_location=$(location) TF_VAR_subnet_name=$(subnet_name) TF_VAR_security_group_name=$(security_group_name) TF_VAR_nic_ipconfig_name=$(nic_ipconfig_name) TF_VAR_vm_size=$(vm_size) TF_VAR_vm_admin_username=$(vm_admin_username) TF_VAR_vm_admin_password=$(vm_admin_password) TF_VAR_iot_edgedevice=$(iot_edgedevice) && \
    terraform plan -destroy -no-color

destroy: init
	@echo "running terraform destroy"
	cd $(LOCATION) && \
	export ARM_SUBSCRIPTION_ID=$(ARM_SUBSCRIPTION_ID) && \
    export ARM_TENANT_ID=$(ARM_TENANT_ID) && \
    export ARM_CLIENT_ID=$(ARM_CLIENT_ID) && \
    export ARM_CLIENT_SECRET=$(ARM_CLIENT_SECRET) && \
    export TF_VAR_name=$(name) TF_VAR_location=$(location) TF_VAR_subnet_name=$(subnet_name) TF_VAR_security_group_name=$(security_group_name) TF_VAR_nic_ipconfig_name=$(nic_ipconfig_name) TF_VAR_vm_size=$(vm_size) TF_VAR_vm_admin_username=$(vm_admin_username) TF_VAR_vm_admin_password=$(vm_admin_password) TF_VAR_iot_edgedevice=$(iot_edgedevice) && \
    terraform destroy -auto-approve -no-color